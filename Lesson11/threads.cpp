#include "threads.h"
#include <thread>

//Function prints "I love threads"
void I_Love_Threads()
{
	cout << "I Love Threads!" << endl;
}

//Function Printing "I Love Threads" using a thread
void call_I_Love_Threads()
{
	thread trd = thread(I_Love_Threads);
	trd.join();
}

/*
	Function inserting all of the prime numbers in the given range into the given vector
	input: starting number, ending number, vector
	output: None
*/
void getPrimes(int begin, int end, vector<int>& primes)
{
	for (int i = begin; i <= end; i++)
	{
		bool isPrime = true;
		for (int j = 2; j < round(sqrt(i)) && isPrime; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
			}
		}

		if (isPrime)
		{
			primes.push_back(i);
		}
	}
}


/*
	Function returns a vector with all prime numbers in given range
	input: begin, end
	output: vector
*/
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> res;
	clock_t beginT = clock();
	thread trd = thread(getPrimes, begin, end, ref(res));
	trd.join();
	clock_t dur = (clock() - beginT) / CLOCKS_PER_SEC;
	cout << "Time to thread: " << dur << " seconds" << endl;
	return res;
}

//Function prints given vector to the screen
void printVector(vector<int> primes)
{
	for (auto val : primes)
	{
		cout << val << endl;
	}
}

/*
	Function writes to given file all of the prime numbers in the given range
	input: range (begin, end), file
	output: none
*/
void writePrimesToFile(int begin, int end, ofstream & file)
{
	for (int i = begin; i <= end; i++)
	{
		bool isPrime = true;
		for (int j = 2; j < round(sqrt(i)) && isPrime; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
			}
		}

		if (isPrime)
		{
			file << i << "\n";
		}
	}
}

/*
	Function divides given range into N sections, and starting threads on each
	sub-range that prints the prime numbers in that range to the given file
	input: begin, end, file path, number of sections
	output: None (only to file)
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	thread* trds = new thread[N];
	int rangeLen = round((end - begin) / N);
	
	ofstream file;
	file.open(filePath);

	clock_t beginT = clock();
	int idx = 0;
	for (int i = begin; i <= end - rangeLen; i += rangeLen)
	{
		cout << i << ", " << i + rangeLen << endl;
		trds[idx] = thread(writePrimesToFile, i, i + rangeLen, ref(file));
		idx++;
		i++;
	}
	for (int i = 0; i < N; i++)
	{
		trds[i].join();
		clock_t dur = clock() - beginT;
		cout << "Tread " << i << " Duration: " << double(dur / CLOCKS_PER_SEC) << endl;
	}
}