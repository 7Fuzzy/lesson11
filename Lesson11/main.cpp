#include "threads.h"



int main()
{
	call_I_Love_Threads();

	vector<int> primes1;
	cout << "Checking 1 - 100:" << endl;
	getPrimes(1, 100, primes1);
	printVector(primes1);
	cout << "Checking 1 - 1000000:" << endl;
	vector<int> primes3 = callGetPrimes(1, 1000000);
	//printVector(primes3);

	callWritePrimesMultipleThreads(1, 100000, "primes2.txt", 2);

	system("pause");
	return 0;
}